const app   =   angular.module('Test', ['ngRoute']);

app.controller('data', function($scope, $http){

    var list= [];
    var user    =   '';
    var items   =   '';
    $(document).ready(function(){
        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
        $('.tooltipped').tooltip();
      });
           
    
    this.getData    =   function(){
        console.log('hola mundo');
        const url = 'src/asset/dataList.json';

        $http.get(url).then((response)=> {
            console.log(response.data);
            //$scope.list =   response.data;
            localStorage.setItem('data', JSON.stringify(response.data));
            console.log('storahe', localStorage.getItem('data'));
            $scope.list = JSON.parse(localStorage.getItem('data'));
        })
        
    }

    this.deleteData =   function(item){
        console.log(item);
        $scope.list.splice(item,1 );
        localStorage.setItem('data', JSON.stringify($scope.list));
        console.log('storahe', localStorage.getItem('data'));
    }

    this.editData    =   function(item){
        $scope.user =   $scope.list[item];
        $scope.items    =   item;
        //$scope.user =   'hola mundo';
        console.log($scope.user);
    }

    this.putData    =   function(name, full_name){
        
        $scope.list[$scope.items].name = name;
        $scope.list[$scope.items].full_name =   full_name;
        localStorage.setItem('data', JSON.stringify($scope.list));
        console.log($scope.list[$scope.items]);
    }

    this.add    =   function(){
        $scope.list.push({
            name:   $scope.name,
            full_name: $scope.full_name
        });

        localStorage.setItem('data', JSON.stringify($scope.list));

    }
});